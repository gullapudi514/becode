package com.becode.service;

import com.becode.model.MasterJson;

import java.util.List;

public interface ExternalService {
    List<MasterJson> findAllATMs();
    List<MasterJson> findATMsByCity(String city);
}
