package com.becode.service.impl;

import com.becode.controller.ATMController;
import com.becode.model.MasterJson;
import com.becode.service.ExternalService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ExternalServiceImpl implements ExternalService {
    private static  final Logger LOGGER = LoggerFactory.getLogger(ExternalServiceImpl.class);
    private final String baseURL = "https://www.ing.nl/api/locator/atms/";

    @Override
    public List<MasterJson> findAllATMs() {
        RestTemplate restTemplate = new RestTemplate();
        String json = restTemplate.getForObject(baseURL, String.class);
        json = json.substring(5);

        ObjectMapper mapper = new ObjectMapper();
        List<MasterJson> addressList = new ArrayList<>();
        try {
            addressList = mapper.readValue(json, new TypeReference<List<MasterJson>>() {
            });
        } catch (JsonProcessingException e) {
           LOGGER.error("Error while parsing .. "+e);
        }
        return addressList;
    }

    @Override
    public List<MasterJson> findATMsByCity(String city) {
        List<MasterJson> addressList = findAllATMs();

        return addressList.stream().filter(masterJson -> masterJson.getAddress().getCity().equalsIgnoreCase(city)).collect(Collectors.toList());

    }
}
