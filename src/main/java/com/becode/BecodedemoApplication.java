package com.becode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootApplication
public class BecodedemoApplication {


	public static void main(String[] args) {
		SpringApplication.run(BecodedemoApplication.class, args);
	}

}
