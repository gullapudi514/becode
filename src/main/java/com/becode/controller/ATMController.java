package com.becode.controller;

import com.becode.model.MasterJson;
import com.becode.service.ExternalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ATMController {
    @Autowired
    ExternalService externalService;

    @GetMapping(value = "/findAllATMs",produces ="application/json")
    public ResponseEntity<List<MasterJson>> listAtms() {

        List<MasterJson> addressList = externalService.findAllATMs();

        return ResponseEntity.ok(addressList);

    }

    @GetMapping(value= "/findATMByCity",produces ="application/json")
    public ResponseEntity<List<MasterJson>> getAtmsByCity(@RequestParam(required = false) String city) {
        List<MasterJson> addressList = externalService.findATMsByCity(city);

        return !addressList.isEmpty()?ResponseEntity.ok(addressList): new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
