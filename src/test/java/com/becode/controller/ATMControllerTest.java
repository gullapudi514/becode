package com.becode.controller;

import com.becode.model.MasterJson;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ATMControllerTest {
    @Test
    void testGetAllATMs(){
        RestTemplate restTemplate = new RestTemplate();
        String json = restTemplate.getForObject("https://www.ing.nl/api/locator/atms/", String.class);
        json = json.substring(5);

        ObjectMapper mapper = new ObjectMapper();

        List<MasterJson> addressList = new ArrayList<>();

        try {
            addressList = mapper.readValue(json, new TypeReference<List<MasterJson>>() {
            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        //Verify request succeed
        Assert.assertNotNull(addressList);
    }

    @Test
    void testFindATMByCity(){
        RestTemplate restTemplate = new RestTemplate();
        String json = restTemplate.getForObject("https://www.ing.nl/api/locator/atms/", String.class);
        json = json.substring(5);

        ObjectMapper mapper = new ObjectMapper();

        List<MasterJson> addressList = new ArrayList<>();

        try {
            addressList = mapper.readValue(json, new TypeReference<List<MasterJson>>() {
            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        addressList.stream().filter(masterJson -> masterJson.getAddress().getCity().equalsIgnoreCase("Zuidhorn")).collect(Collectors.toList());
        //Verify request succeed
        Assert.assertNotNull(addressList);
    }
}
